const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    console.log(token);
    const decoded = jwt.verify(token, process.env.JWT_KEY);
    req.userDate = decoded;
    next();
  } catch (err) {
    const error = new Error("Unauthorized request");
    error.status = 401;
    next(error);
  }
};
