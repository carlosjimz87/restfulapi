const express = require("express");
const router = express.Router();
const multer = require("multer");
const checkAuth = require("../middleware/check-auth");

const ProductsController = require("../controllers/products");

// configuring folder and imagename
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./uploads/");
  },
  filename: function (req, file, cb) {
    cb(null, new Date().toISOString() + file.originalname);
  },
});
// filtering the files to upload
const filterFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
// configuring uploader multer
const upload = multer({
  fileFilter: filterFilter,
  storage: storage,
  limits: { fileSize: 1024 * 1024 * 5 },
});

router.get("/", ProductsController.products_get_all);

router.get("/:productId", ProductsController.products_get_one);

router.patch("/:productId", checkAuth, ProductsController.products_update);

router.delete("/:productId", checkAuth, ProductsController.products_delete);

router.post(
  "/",
  checkAuth,
  upload.single("productImage"),
  ProductsController.products_create
);

module.exports = router;
