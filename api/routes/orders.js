const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();
const checkAuth = require("../middleware/check-auth");

const Order = require("../models/orders");
const Product = require("../models/products");

const OrdersController = require("../controllers/orders");

router.get("/", checkAuth, OrdersController.orders_get_all);

router.get("/:orderId", checkAuth, OrdersController.order_get_one);

router.delete("/:orderId", checkAuth, OrdersController.order_delete);

router.post("/", checkAuth, OrdersController.order_create);

module.exports = router;
