const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const bodyParser = require("body-parser");

const productRoutes = require("./api/routes/products");
const orderRoutes = require("./api/routes/orders");
const userRoutes = require("./api/routes/user");
const errorHandler = require("./api/_helpers/error-handler");
const app = express();

// setting loging with morgan
app.use(morgan("dev"));
// enabling the static folder uploads/
app.use("/uploads", express.static("uploads"));
// setting parsing for urlencoded and json bodies
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);
app.use(bodyParser.json());

// preventing CORSS errors
app.use(cors());

// handling routes
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);
app.use("/user", userRoutes);

// default route
app.use((req, res, next) => {
  const error = new Error("Endpoint not found");
  error.status = 404;
  next(error);
});

// handling errors
app.use(errorHandler);

module.exports = app;
